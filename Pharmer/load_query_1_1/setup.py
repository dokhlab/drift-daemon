#!/usr/bin/python

#script to install the load_query pymol module
#It moves the script to the specified location and then edits the .pymolrc file
#by Matthew Baumgartner
#8-9-12

#License:
#---------
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import sys
import os
import shutil

def main():
    '''
    Install the load_query pymol script into the user specified directory (Default = cwd).
    And create or edit the .pymolrc config file so the script is auto loaded.
    '''
    if not len(sys.argv) == 2 or sys.argv[1] != 'install':
        sys.stderr.write('Install the load_query pymol script and edit or create the .pymolrc file\n')
        sys.stderr.write('usage: python setup.py install\n')
        sys.exit()
    else:
        print 'Starting setup for load_query'
        #get the current working directory
        cwd = os.getcwd() + '/'
        
        if not os.path.exists(cwd + 'load_query.py'):
            sys.stderr.write('load_query.py script needs to be in the cwd\nExiting!\n')
            sys.exit(1)
        
        print 'load_query script currently in:', cwd
        installdir = raw_input('Input desired install directory (leave blank for current location): ')
        
        if installdir == '':
            installdir = cwd
        
        if not os.path.exists(os.path.abspath(installdir)):
            sys.stderr.write('Specified Folder: ' + os.path.abspath(installdir) + ' not found!\n')
            mkdir = ''
            while mkdir not in ['y', 'n']:
                mkdir = raw_input('Create new directory? [y]/n: ')
            
            if mkdir == 'n':
                print 'Directory not created. Exiting!'
                sys.exit()
            elif mkdir == 'y' or mkdir == '':
                print 'Attempting to create directory:', installdir
                
                os.mkdir(installdir)
        
        if not os.path.isdir(installdir):
            sys.stderr.write('Specified installation path is not a valid directory: ' + installdir + '\n')
        
        #make sure there is a trailing '/' at the end of the install dir path
        if installdir[-1] != '/':
            installdir += '/'
            
        #if you are installing it elsewhere, copy it to there
        if not os.path.samefile(cwd, installdir):
            print 'Copying file to:', installdir
            shutil.move('load_query.py', installdir)
        
        
        #Now create if needed and then edit the .pymolrc file
        
        #get the users home directory
        homedir = os.getenv("HOME") + '/'
        
        pymolrcfile = homedir + '.pymolrc'
        
        #if the .pymolrc file doesn't exist, ask the user if you should make it
        if not os.path.exists(pymolrcfile):
            print 'Pymol config file not found:', pymolrcfile
            makerc = ''
            
            while makerc not in ['y', 'n', '']:
                makerc = raw_input('Create file:' + pymolrcfile + '? [y]/n: ')
            
            if makerc == 'n':
                print 'Pymol config file not created.'
                print 'You will need to manually start the load_query script in pymol every time'
                print 'Do this by running the command `run /path/to/script/load_query.py` in pymol'
                print 'Exiting!'
                sys.exit()
            elif makerc in ['y', '']:
                print 'Creating .pymolrc file:', pymolrcfile
                #open and close the file
                open(pymolrcfile, 'w').close()
    
        #create a backup of the pymolrc file
        else:
            print 'Creating back up of .pymolrc file'
            shutil.copy(pymolrcfile, homedir + 'old.pymolrc')
            
        print 'Writing to the .pymolrc file'
        #Edit the .pymolrc file and add the run commmand
        pymolrc = open(pymolrcfile, 'a')
        pymolrc.write("\n\n#Lines below added by load_query setup script \n#load the load_query command\nrun " + installdir + 'load_query.py\n#Lines above added by load_query setup script\n')
        pymolrc.close()
        
        print 'Finished writing to:', pymolrcfile
        print 'Setup Completed Successfully!'
            
    
    





if __name__ == '__main__':
    main()