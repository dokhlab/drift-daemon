<?php

ini_set('memory_limit', '1024M'); // !!! IMPORTANT

require_once(__DIR__.'/mail_utils.php');
require_once(__DIR__.'/config.php');
require_once(__DIR__.'/chembl_utils.php');

use Spatie\Async\Pool;

$query = "SELECT td.chembl_id as target, cs.sequence, md.chembl_id as compound, ct.canonical_smiles as smiles, act.standard_value as kd FROM (SELECT assay_id, molregno, standard_value from activities act where standard_type='Kd' and CONCAT('', standard_value*1)=standard_value and standard_units='nM' ORDER BY RAND() LIMIT 5000) act INNER JOIN assays a ON act.assay_id = a.assay_id INNER JOIN molecule_dictionary md ON md.molregno=act.molregno INNER JOIN target_dictionary td ON td.tid=a.tid INNER JOIN target_components tc ON td.tid=tc.tid INNER JOIN component_sequences cs ON cs.component_id=tc.component_id INNER JOIN compound_structures ct ON ct.molregno=act.molregno";

#$query = "SELECT td.chembl_id as target, cs.sequence, md.chembl_id as compound, ct.canonical_smiles as smiles FROM target_dictionary td JOIN target_components tc ON td.tid=tc.tid JOIN component_sequences cs ON tc.component_id=cs.component_id JOIN assays a ON td.tid = a.tid JOIN activities act ON a.assay_id = act.assay_id JOIN molecule_dictionary md ON md.molregno = act.molregno JOIN compound_structures ct ON ct.molregno = md.molregno GROUP BY a.tid LIMIT 1";

#  $query = "SELECT td.chembl_id, td.target_type, td.pref_name, td.organism, act.standard_value, act.standard_units, act.standard_type, a.chembl_id as assay_id FROM target_dictionary td JOIN assays a ON td.tid = a.tid JOIN activities act ON a.assay_id = act.assay_id JOIN molecule_dictionary md ON md.molregno = act.molregno AND md.chembl_id = '$compound_id' JOIN compound_properties cp ON cp.molregno = md.molregno GROUP BY td.chembl_id;";

#        $query = "SELECT cp.full_mwt, cp.full_molformula, cs.canonical_smiles, GROUP_CONCAT(DISTINCT ms.synonyms SEPARATOR '; ') as synonyms FROM molecule_dictionary md JOIN molecule_synonyms ms ON ms.molregno = md.molregno JOIN compound_structures cs ON cs.molregno = md.molregno JOIN compound_properties cp ON cp.molregno = md.molregno AND md.chembl_id = '{$compound->id}';";

$rows = chembl_fetch($query);
#var_dump($rows);
foreach ($rows as $row) {
  echo sprintf("%s %s %s %s %s\n", $row['target'], $row['sequence'], $row['compound'], $row['smiles'], $row['kd']);
}
