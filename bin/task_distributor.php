<?php

require_once(__DIR__.'/config.php');

#echo "Distributing...\n";

$conn = database_connect();

// Submit drift tasks
$query = "select * from drift_tasks where status<>'finished' and status<>'failed'";

$stmt = $conn->prepare($query); 
$stmt->execute(); 
$rows = $stmt->fetchAll();

foreach ($rows as $k => $v) {
    $id = $v['id'];
#    echo "Handle ID: $id\n";
    $n = intval(shell_exec("ps aux | grep 'php drift.php $id' | grep -v grep | wc -l"));
#    echo "$n threads are running to process Task $id.\n";
    $num_total_runnings = intval(shell_exec("ps aux | grep 'php drift.php' | grep -v grep | wc -l"));
    if ($num_total_runnings < $max_tasks && $n == 0) {
        echo "Totally, $num_total_runnings threads are running.\n";
        echo "Submit SimComp Task $id.\n";
        pclose(popen("php drift.php $id >".__DIR__."/../logs/$id.log 2>&1 &", 'r'));
    }
}

// Stop tasks
$query = "select * from drift_tasks where status='stopping'";

$stmt = $conn->prepare($query);                        
$stmt->execute();   
$rows = $stmt->fetchAll();

foreach ($rows as $k => $v) {
    $id = $v['id'];
#    echo "Task $id are labeled to be stopped.\n";
    $n = intval(shell_exec("ps aux | grep 'php drift.php $id' | grep -v grep | wc -l"));
#    echo "$n threads are used to process Task $id.\n";
    if ($n != 0) {
      echo "Stopping $id\n";
      $query = "update drift_tasks set status='failed' where id='$id'";
      shell_exec('kill $(ps aux | grep -v grep | grep "php drift.php '.$id.'" | perl -lane \'print $F[1]\')');
      $conn->exec($query);
    }
}

$conn = null;

