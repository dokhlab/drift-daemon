DIR=$(cd $(dirname $0); pwd)

n=$(ps aux | grep 'run_drift' | grep -v grep | wc -l)

if [[ $n -lt 0 ]]; then
  echo "There is already an 'run_drift' instance running in the system."
  exit 0;
fi

cd $DIR

while true; do
    php task_distributor.php
    sleep 3
done

cd -
