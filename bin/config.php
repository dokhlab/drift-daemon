<?php

require_once(__DIR__.'/mail_utils.php');

$data_path = "/data/drift";
$drift_path = "/usr/local/drift";
// $dti_path = "$drift_path/DeepConv-DTI/pred.sh";

#$config      = yaml_parse(file_get_contents('/data/config.yaml'));
#$config      = json_decode(file_get_contents("$data_path/config.json"), true);

$dbhost      = $_ENV['DBHOST'];
$dbuser      = $_ENV['DBUSER'];
$dbpass      = $_ENV['DBPASS'];

$babel_path  = "babel";

$max_tasks   = 16;

function database_connect() {
    global $dbhost, $dbuser, $dbpass;

    $conn = new PDO("mysql:host=$dbhost;dbname=dokhlab", $dbuser, $dbpass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
}

function chembl_connect() {
    global $dbhost, $dbuser, $dbpass;
    $conn = new PDO("mysql:host=$dbhost;dbname=chembl_25", $dbuser, $dbpass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
}

function database_execute($conn, $query) {
    $stmt = $conn->prepare($query); 
    return $stmt->execute(); 
}

function database_fetch($conn, $query) {
    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $row;
}

function chembl_execute($query) {
    $conn = chembl_connect();

    $stmt = $conn->prepare($query); 
    $r = $stmt->execute(); 

    $conn = null;

    return $r;
}

function chembl_fetch($query) {
    $conn = chembl_connect();

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $row = $stmt->fetchall(PDO::FETCH_ASSOC);
    $conn = null;

    return $row;
}

function save_file($file) {
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    $name = tempnam(__DIR__."/../blobs", '').".$ext";
    $cmd = "cp $file $name; chmod 777 $name";
    system($cmd);
    $name = pathinfo($name, PATHINFO_BASENAME);
    return $name;
}
