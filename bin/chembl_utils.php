<?php

ini_set('memory_limit', '1024M'); // !!! IMPORTANT

require_once(__DIR__.'/config.php');

function get_chembl_target($id) {
#  $cache_table = "target_cache";
#
#  // Create table
#  $query = "create table if not exists $cache_table (id varchar(100), target MEDIUMTEXT, update_time timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP);";
#  chembl_execute($query);
#
#  $target = [];
#
#  $query = "select target from $cache_table where id='$id'";
#  $rows = chembl_fetch($query);
#
#  if ($rows) {
#    $target_doc = $rows[0]['target'];
#    if ($target_doc != "") {
#      return json_decode($target_doc, true);
#    }
#  }

  // Get the target from the website
  $url = "https://www.ebi.ac.uk/chembl/api/data/target/$id.json";
  $target_doc = file_get_contents($url);
  $target = json_decode($target_doc, true);

#  // Insert data
#  $conn = chembl_connect();
#  $id_str = $conn->quote($id);
#  $target_str = $conn->quote($target_doc);
#  $query = "replace into $cache_table (id, target) values ($id_str, $target_str);";
#  database_execute($conn, $query);
#  $conn = null;
#  var_dump(array_keys($target));
  return $target;
}

