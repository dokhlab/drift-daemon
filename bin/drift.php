<?php

ini_set('memory_limit', '1024M'); // !!! IMPORTANT

require_once(__DIR__.'/mail_utils.php');
require_once(__DIR__.'/config.php');
require_once(__DIR__.'/chembl_utils.php');

use Spatie\Async\Pool;

$id = $argv[1];
$email = '';

function update_task($id, $m1, $m2 = []) {
    $conn = database_connect();

    $query = "update drift_tasks set ";
    $tmp = [];
    foreach ($m1 as $k => $v) {
        $q = $conn->quote($v);
        $tmp[] = "$k=$q";
    }
    foreach ($m2 as $k => $v) {
        $q = $v;
        $tmp[] = "$k=$q";
    }
    $query .= implode(',', $tmp);
    $query .= " where id=$id";

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $conn = null;
}

function insert_result($taskid, $icompound, $result) {
    $conn = database_connect();

    $result = $conn->quote($result);
    $query = "replace into drift_results set taskid=$taskid, icompound=$icompound, result=$result, tfinish=CURRENT_TIMESTAMP()";

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $conn = null;
}

function addnew(&$array, $value) {
  if (!in_array($value, $array)) {
    $array[] = $value;
  }
}

class Par {
  public $host = "https://dokhlab.med.psu.edu";
  public $num = 5;
  public $compDb = ["CHEMBL"];
  public $simCutoff = 0.85;
  public $fpType = 'FP2';
  public $fragmentation = false;

  public $id;
  public $email;
  public $log_file;
}

class Task {
  // Outputs
  public $smi;
  public $work;
  public $input = 'input.sdf';

  public $compounds = [];
  public $assays = [];
  public $targets = [];
  public $functions = [];
  public $processes = [];
  public $fragments = [];
}

class Compound {
  public $db;
  public $id;
  public $smiles; # smiles
  public $mw; # molecular weight
  public $formula;
  public $synonyms;
  public $score;

  public $assays = [];
}

class Target {
  public $db;
  public $id;

  public $score;
  public $cpi; // CPI prediction score
  public $type;
  public $pref_name;
  public $organism;
  public $seq;
  public $assays = [];
  public $functions = [];
  public $processes = [];
  public $accession = [];
}

class TargetFunction {
  public $id;
  public $name;
  public $targets = [];
}

class TargetProcess {
  public $id;
  public $name;
  public $targets = [];
}

class Assay {
  public $id;
  public $compound;
  public $target;
  public $act_value;
  public $act_units;
  public $act_type;
}

function set_inputs($email, $id) {
  global $data_path;

  echo "Fetch Inputs of $id...\n";

  $conn = database_connect();

  $query = "select * from drift_tasks where id = :id";

  $stmt = $conn->prepare($query); 
  $stmt->execute(array('id' => $id)); 
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  if (!empty($row)) {
    // Create execution directories and input files
    $input_path = "$data_path/exec/$id/input";
    if (!file_exists($input_path)) {
      echo "Making directory: $input_path...\n";
      mkdir($input_path, 0777, true);
    }

    foreach ($row as $k => $v) {
      file_put_contents("$input_path/$k", $v);
    }

    $userid = $row['userid'];
    $query = "select id, email from users where id=:id";

    $stmt = $conn->prepare($query); 
    $stmt->execute(array('id' => $userid)); 
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!empty($row)) {
      $email = $row['email'];
    }

    return true;
  }

  $conn = null;
}

function send_result_mail($id, $email, $host, $success) {
  if (!empty($email)) {
    if ($success) {
      $subject = "SimComp Task Finished";
      $message  = "Dear User,\n\n";
      $message .= "Thank you for using SimComp! Your Task $id has been finished. Please click this link ($host/drift/#/Queue) for details!\n\n";
      $message .= "Sincerely,\n";
      $message .= "SimComp Team";
    } else {
      $subject = "SimComp Task Failed";
      $message  = "Dear User,\n\n";
      $message .= "Thank you for using SimComp! Your Task $id has failed. Please click this link ($host/drift/#/Queue) for details!\n\n";
      $message .= "Sincerely,\n";
      $message .= "SimComp Team";
    }
    send_mail($email , $subject , $message);
  }
}

function find_functions_processes($par, $task) {
  $pool = Pool::create();
  foreach ($task->targets as $targetid => $target) {
    $pool->add(function () use ($targetid) {
      $target_info = [];

      try {
        $functions = [];
        $processes = [];

        $uniprots = [];

        $url = "https://www.ebi.ac.uk/chembl/api/data/target/$targetid.json";
        $content = @file_get_contents($url);

        if (isset($content) && trim($content) !== '') {
          $target_doc = json_decode($content, true);

          $components = $target_doc["target_components"];
          if (count($components) > 0) {
            foreach ($components as $component) {
              foreach ($component["target_component_xrefs"] as $ref) {
                if ($ref["xref_src_db"] == "GoFunction") {
                  array_push($functions, ["id"=>$ref["xref_id"], "name"=>$ref["xref_name"]]);
                } else if ($ref["xref_src_db"] == "GoProcess") {
                  array_push($processes, ["id"=>$ref["xref_id"], "name"=>$ref["xref_name"]]);
                } else if ($ref["xref_src_db"] == "UniProt") {
                  array_push($uniprots, $ref["xref_id"]);
                }
              }
            }
          }
          unset($target_doc);

          $fasta = "";
          if (count($uniprots) > 0) {
            $url = "https://www.uniprot.org/uniprot/{$uniprots[0]}.fasta";
            $fasta = file_get_contents($url);
          }

          $target_info['seq']       = $fasta;
          $target_info['functions'] = $functions;
          $target_info['processes'] = $processes;
        } else {
          $target_info['seq']       = '';
          $target_info['functions'] = [];
          $target_info['processes'] = [];
        }
      } catch (Exception $e) {
        echo $e->getMessage();
      }
      return $target_info;
    }, 1024*1024)->then(function ($target_info) use ($task, $target) {
      $target_id = $target->id;

      $target->seq = $target_info['seq'];
      foreach ($target_info['functions'] as $func) {
        $func_id = $func['id'];

        if (!array_key_exists($func_id, $task->functions)) {
          // new function
          $function = new TargetFunction;
          $function->id = $func_id;
          $function->name = $func['name'];
          $task->functions[$func_id] = $function;
        }

        addnew($task->functions[$func_id]->targets, $target_id);
        addnew($target->functions, $func_id);
      }
      foreach ($target_info['processes'] as $proc) {
        $proc_id = $proc['id'];

        if (!array_key_exists($proc_id, $task->processes)) {
          // new process
          $process = new TargetProcess;
          $process->id = $proc_id;
          $process->name = $proc['name'];
          $task->processes[$proc_id] = $process;
        }

        addnew($task->processes[$proc_id]->targets, $target_id);
        addnew($target->processes, $proc_id);
      }
    });
  }
  $pool->wait();
}

function find_functions_processes_v2($par, $task) {
  foreach ($task->targets as $targetid => $target) {
    $query = "SELECT td.chembl_id, td.target_type, td.pref_name, td.organism, cs.component_type, cs.accession, cs.sequence, cs.db_source, gc.go_id, gc.pref_name as go_name, gc.aspect as go_aspect FROM target_dictionary td JOIN target_components tc ON td.tid=tc.tid JOIN component_sequences cs ON tc.component_id=cs.component_id JOIN component_go cg ON tc.component_id=cg.component_id JOIN go_classification gc ON cg.go_id=gc.go_id where td.chembl_id='$targetid'";
    $rows = chembl_fetch($query);
    $target->seq = '';
    foreach ($rows as $row) {
      if ($row['go_aspect'] == 'P') {
        $proc_id = $row['go_id'];
        $proc_name = $row['go_name'];

        if (!array_key_exists($proc_id, $task->processes)) {
          // new process
          $process = new TargetProcess;
          $process->id = $proc_id;
          $process->name = $proc_name;
          $task->processes[$proc_id] = $process;
        }

        addnew($target->processes, $proc_id);
      } elseif ($row['go_aspect'] == 'F') {
        $func_id = $row['go_id'];
        $func_name = $row['go_name'];

        if (!array_key_exists($func_id, $task->functions)) {
          // new function
          $function = new TargetFunction;
          $function->id = $func_id;
          $function->name = $func_name;
          $task->functions[$func_id] = $function;
        }

        addnew($target->functions, $func_id);
      }

      if ($target->seq == '') {
        $target->seq = $row['sequence'];
      }

      $accession_id = $row['accession'];
      if (!array_key_exists($accession_id, $target->accession)) {
        $target->accession[$accession_id] = ['id'=>$accession_id, 'db'=>$row['db_source']];
      }
    }
  }
}

function chembl_find_assays($task, &$compound) {
  $compound_id = $compound->id;
  $assays = [];

  $query = "SELECT td.chembl_id, td.target_type, td.pref_name, td.organism, act.standard_value, act.standard_units, act.standard_type, a.chembl_id as assay_id FROM target_dictionary td JOIN assays a ON td.tid = a.tid JOIN activities act ON a.assay_id = act.assay_id JOIN molecule_dictionary md ON md.molregno = act.molregno AND md.chembl_id = '$compound_id' JOIN compound_properties cp ON cp.molregno = md.molregno GROUP BY td.chembl_id;";
  $rows = chembl_fetch($query);
  $pool = [];
  foreach ($rows as $row) {
    $target_id = $row['chembl_id'];
    $assay_id = $row['assay_id'];

    if (!array_key_exists($assay_id, $task->assays)) {
      $assay = new Assay;
      $assay->id = $assay_id;
      $assay->compound = $compound_id;
      $assay->target = $target_id;
      $assay->act_value = $row['standard_value'];
      $assay->act_units = $row['standard_units'];
      $assay->act_type = $row['standard_type'];

      $task->assays[$assay_id] = $assay;
    }

    if (!array_key_exists($target_id, $task->targets)) {
      $target = new Target;
      $target->id = $target_id;
      $target->type      = $row['target_type'];
      $target->pref_name = $row['pref_name'];
      $target->organism  = $row['organism'];
      $target->assays[]  = $assay_id;

      $task->targets[$target_id] = $target;
    }

    addnew($task->targets[$target_id]->assays, $assay_id);
    addnew($task->compounds[$compound_id]->assays, $assay_id);
  }
//  return $assays;
}

function hmdb_find_targets($task, $compound, &$targets, &$cached_functions, &$cached_processes) {
  global $drift_path;

  $targets = [];

  $xml_file = "$drift_path/lib/hmdb_metabolites/$_id.xml";

  if (!file_exists($xml_file)) {
    $mw = '';
    $formula = '';
    $targets = [];

    return [
      'database'=>$db,
      'id'=>$_id,
      'score'=>$scores[$i],
      'mw'=>$mw,
      'formula'=>$formula,
      'targets'=>$targets
    ];
  } else {
    $xml=simplexml_load_file($xml_file) or die("Cannot open $xml_file!");

    $mw = strval($xml->average_molecular_weight);
    $formula = strval($xml->chemical_formula);
    $smiles = strval($xml->smiles);

    $targets = [];
    $proteins = $xml->protein_associations->protein;
    if (count($proteins) > 0) {
      foreach ($proteins as $protein) {
        $protein_id = strval($protein->protein_accession);
        echo "Target: $protein_id\n";

        $xml_file = "$drift_path/lib/hmdb_proteins/$protein_id.xml";

        if (file_exists($xml_file)) {
          $xml=simplexml_load_file($xml_file) or die("Cannot open $xml_file!");

          $gos = $xml->go_classifications->go_class;
          $functions = [];
          $processes = [];
          if (count($gos) > 0) {
            foreach ($gos as $go) {
              $category = strval($go->category);
              if ($category == "Biological process") {
                $process = strval($go->description);
                $go_id = strval($go->go_id);
                array_push($processes, ['id'=>$go_id, 'name'=>$process]);
              } else if ($category == "Molecular function") {
                $function = strval($go->description);
                $go_id = strval($go->go_id);
                array_push($functions, ['id'=>$go_id, 'name'=>$function]);
              }
            }
          }

          $target = [
            'id'=>$protein_id,
            'pref_name'=>strval($protein->name),
            'uniprot_id'=>strval($protein->uniprot_id),
            'type'=>strval($protein->protein_type),
            'functions'=>$functions,
            'processes'=>$processes
          ];
          array_push($targets, $target);
        }
      }
    }

    return [
      'database'=>$db,
      'id'=>$_id,
      'score'=>$scores[$i],
      'mw'=>$mw,
      'formula'=>$formula,
      'smiles'=>$smiles,
      'targets'=>$targets
    ];
  }

  return $targets;
}

function zinc_find_targets($task, $compound, &$targets, &$cached_functions, &$cached_processes) {
  $targets = [];

  $mw = '';
  $formula = '';
  $targets = [];

  return [
    'database'=>$db,
    'id'=>$_id,
    'score'=>$scores[$i],
    'mw'=>$mw,
    'formula'=>$formula,
    'targets'=>$targets
  ];

  return $targets;
}

function bindingdb_find_targets($task, $compound, &$targets, &$cached_functions, &$cached_processes) {
  $targets = [];

  $mw = '';
  $formula = '';
  $targets = [];

  return [
    'database'=>$db,
    'id'=>$_id,
    'score'=>$scores[$i],
    'mw'=>$mw,
    'formula'=>$formula,
    'targets'=>$targets
  ];

  return $targets;
}

function eachline($filename) {
  if (file_exists($filename)) {
    $handle = fopen($filename, "r");
    if ($handle) {
      $iline = 0;
      while (($line = fgets($handle)) !== false) {
        yield $iline => $line;
        $iline += 1;
      }
      fclose($handle);
    }
  }
}

function pharm_search($par, $task) {
  global $drift_path;

  $work = $task->work;

  $script = "$drift_path/scripts/pharm-search.sh";
  $infile = "$work/input.smi";
  $lib = "$drift_path/lib/chembl_25-pharm";
  $outfile = "$work/pharmsearch-out.txt";

  $cmd = "cd $work; bash $script $infile $lib $outfile";
  shell_exec($cmd);

  foreach (eachline($outfile) as $iline => $line) {
    if ($iline < 100) {
      $l = preg_split("/\s+/", $line);
      $compoundid = $l[0];
      $score = intval($l[1]);

      $compound = new Compound;
      $compound->db = "CHEMBL";
      $compound->id = $compoundid;
      $compound->score = $score;

      if ($db == "CHEMBL") {
        $query = "SELECT cp.full_mwt, cp.full_molformula, cs.canonical_smiles, GROUP_CONCAT(DISTINCT ms.synonyms SEPARATOR '; ') as synonyms FROM molecule_dictionary md JOIN molecule_synonyms ms ON ms.molregno = md.molregno JOIN compound_structures cs ON cs.molregno = md.molregno JOIN compound_properties cp ON cp.molregno = md.molregno AND md.chembl_id = '{$compound->id}';";
        $rows = chembl_fetch($query);

        $compound->mw = $rows[0]['full_mwt'];
        $compound->formula = $rows[0]['full_molformula'];
        $compound->smiles = $rows[0]['canonical_smiles'];
        $compound->synonyms = $rows[0]['synonyms'];
      } else if ($db == "Metabolites") {
      } else if ($db == "Zinc") {
      } else if ($db == "BindingDB") {
      }

      if ($compound->score > 10) {
        $task->compounds[$compound->id] = $compound;
      }
    }
  }
}

function fp2_search($par, $task) {
  global $babel_path, $data_path, $drift_path;

  $self_compounds = [];
  $similar_compounds = [];

  $input = "input.sdf";
  $smi = $task->smi;
  $work = $task->work;
  $num = $par->num;
  $log_file = $par->log_file;

#  $cmd = "echo '$smi' >$work/input.smi; $babel_path $work/input.smi $work/input.sdf --gen3D";
  $cmd = "echo '$smi' >$work/input.smi; bash $drift_path/scripts/smi2sdf.sh $work/input.smi $work/input.sdf";
  echo "$smi\n";
  echo "$cmd\n";
  shell_exec($cmd);

  foreach ($par->compDb as $db) {
    if ($db == "CHEMBL") {
      $db_prefix = "chembl_25";
    } else if ($db == "Metabolites") {
      $db_prefix = "metabolites";
    } else if ($db == "Zinc") {
      $db_prefix = "Zinc-activity";
    } else if ($db == "BindingDB") {
      $db_prefix = "BindingDB_All_2D";
    }

    echo "Search for similar compounds ...\n";
    $output = "output-$db.sdf";

    $cmd = "cd $work; $babel_path $drift_path/lib/$db_prefix.fs $output -s $input -at$num >$log_file 2>&1";
    echo $cmd."\n";
    shell_exec($cmd);

    // Get the number of Clusters
    $cmd = 'perl -lane \'$n++ if /\$\$\$\$/;END{print $n}\''." $work/$output";
    echo $cmd."\n";
    $n = intval(shell_exec($cmd));
    echo "$n similar compounds found...\n";

    // Get IDs
    $ids = [];
    if ($db == "CHEMBL") {
      $flag = "chembl_id";
      $cmd = 'perl -lane \'if($flag==1){print $_;$flag=0;}$flag=1 if />  <'.$flag.'>/;\''." $work/$output";
      $bash_results = shell_exec($cmd);
      $ids = preg_split('/\s+/', trim($bash_results));
    } else if ($db == "Metabolites") {
      $flag = "DATABASE_ID";
      $cmd = 'perl -lane \'if($flag==1){print $_;$flag=0;}$flag=1 if />  <'.$flag.'>/;\''." $work/$output";
      $bash_results = shell_exec($cmd);
      $ids = preg_split('/\s+/', trim($bash_results));
    } else if ($db == "Zinc") {
      $cmd = 'perl -lane \'print $F[0] if /^ZINC/\''." $work/$output";
      $bash_results = shell_exec($cmd);
      $ids = preg_split('/\s+/', trim($bash_results));
    } else if ($db == "BindingDB") {
      $cmd = 'perl -lane \'print $F[0] if /OpenBabel/\''." $work/$output";
      $bash_results = shell_exec($cmd);
      $ids = preg_split('/\s+/', trim($bash_results));
    }

    // Get Scores
    $score_file = "score-$db.txt";
    $cmd = "$babel_path $work/$input $work/$output -ofpt".' | perl -lane \'if(/Tanimoto.+= (.+)\s*$/){print $1}\''." >$work/$score_file";
    echo $cmd."\n";
    shell_exec($cmd);
    $scores = array_map(function ($i) {
      return doubleval($i);
    }, preg_split('/\s+/', trim(shell_exec("cat $work/$score_file"))));

    $n = count($ids);
    for ($i = 0; $i < $n; $i++) {
      $compound = new Compound;
      $compound->db = $db;
      $compound->id = $ids[$i];
      $compound->score = $scores[$i];

      if ($db == "CHEMBL") {
        $query = "SELECT cp.full_mwt, cp.full_molformula, cs.canonical_smiles, GROUP_CONCAT(DISTINCT ms.synonyms SEPARATOR '; ') as synonyms FROM molecule_dictionary md JOIN molecule_synonyms ms ON ms.molregno = md.molregno JOIN compound_structures cs ON cs.molregno = md.molregno JOIN compound_properties cp ON cp.molregno = md.molregno AND md.chembl_id = '{$compound->id}';";
        $rows = chembl_fetch($query);

        $compound->mw = $rows[0]['full_mwt'];
        $compound->formula = $rows[0]['full_molformula'];
        $compound->smiles = $rows[0]['canonical_smiles'];
        $compound->synonyms = $rows[0]['synonyms'];
      } else if ($db == "Metabolites") {
      } else if ($db == "Zinc") {
      } else if ($db == "BindingDB") {
      }

      if ($compound->score > $par->simCutoff) {
        $task->compounds[$compound->id] = $compound;

        if ($compound->score == 1) {
          array_push($self_compounds, $compound->id);
        } else {
          array_push($similar_compounds, $compound->id);
        }
      }
    }
  }
  return array($self_compounds, $similar_compounds);
}

function fragment_compounds($par, $task) {
  global $data_path, $drift_path;

  $work = $task->work;

  echo "\nWrite SMILES files of compounds\n";
  $compound_ids = [];
  $pool = Pool::create();
  foreach ($task->compounds as $compound_id => $compound) {
    if (isset($compound->smiles)) {
      $smiles = $compound->smiles;
      if ($smiles != "") {
#        echo "Write $smiles to $work/$compound_id.smi\n";
        array_push($compound_ids, $compound_id);
        $pool->add(function () use ($work, $compound_id, $smiles) {
          file_put_contents("$work/$compound_id.smi", $smiles);
        });
      }
    }
  }
  $pool->wait();

  file_put_contents("$work/compounds.txt", implode("\n", array_map(function ($i) { return "$i.smi"; }, $compound_ids)));

  $cmd = "cd $work; bash $drift_path/scripts/fragment.sh compounds.txt fragment-result.txt";
  shell_exec($cmd);

  $task->fragments = json_decode(file_get_contents("$work/fragment-result.txt"), true);
}

function find_targets_assays($par, $task) {
  foreach ($task->compounds as $compound_id => $compound) {
    $db = $compound->db;

    $targets = [];
    if ($db == "CHEMBL") {
      chembl_find_assays($task, $compound);
    } else if ($db == "Metabolites") {
      hmdb_find_targets($task, $compound, $targets, $cached_functions, $cached_processes);
    } else if ($db == "Zinc") {
      zinc_find_targets($task, $compound, $targets, $cached_functions, $cached_processes);
    } else if ($db == "BindingDB") {
      bindingdb_find_targets($task, $compound, $targets, $cached_functions, $cached_processes);
    }
  }
}

function startswith( $haystack, $needle ) {
  $length = strlen( $needle );
  return substr( $haystack, 0, $length ) === $needle;
}

function fasta2seq($fasta) {
  #    echo "fasta: $fasta\n";
  $lines = explode("\n", trim($fasta));
  if (startswith(trim($lines[0]), ">")) {
    return implode("", array_slice($lines, 1));
  } else {
    return implode("", $lines);
  }
}

function rank_targets($par, $task) {
  global $drift_path;

  $work = $task->work;

  $smi = $task->smi;
  $smi = preg_split("/\s+/", $smi)[0]; // in case the smiles is composed of two words

  # prepare the input file

  $input = "$work/cpi-input.txt";
  $output = "$work/cpi-output.txt";

  $foo = [];
  $f = fopen($input, "w+");
  fwrite($f, "Sequence,SMILES\n");
  foreach ($task->targets as $target_id => $target) {
    if (isset($target->seq)) {
      $seq = fasta2seq($target->seq);
      #            echo "seq: $seq\n";
      if (!empty($seq)) {
        fwrite($f, "$seq,$smi\n");
        $foo[] = $target_id;
      }
    }
  }
  fclose($f);

  # predict
  #  $cmd = "bash /home/share/apps/splip/scripts/cpi.sh $input $output";
  $cmd = "cd $work; python3 $drift_path/deepdrift/deepdrift.py $input $drift_path/deepdrift/model -o $output &>pred.log";
  shell_exec($cmd);

  foreach (eachline($output) as $iline => $line) {
    $l = preg_split("/\s+/", trim($line));
    if (count($l) >= 1 && $l[0] != "") {
      $target_id = $foo[$iline];
      $task->targets[$target_id]->score = $l[0];
      $iline += 1;
    }
  }
}

function init_tasks($par, $id, $email) {
  global $babel_path, $data_path;

  $par->id = $id;
  $par->email = $email;

  update_task($id, ["status"=>"predicting"], ["tprocess"=>"CURRENT_TIMESTAMP()"]);

  $par_           = json_decode(file_get_contents("$data_path/exec/$id/input/par"), true);

  if (array_key_exists('num', $par_)) $par->num = intval($par_['num']);
  if (array_key_exists('database', $par_)) $par->compDb = $par_['database'];
  if (!is_array($par->compDb)) $par->compDb = [$par->compDb];
  if (array_key_exists('simCutoff', $par_)) $par->simCutoff = doubleval($par_['simCutoff']);
  if (array_key_exists('fpType', $par_)) $par->fpType = $par_['fpType'];
  if (array_key_exists('fragmentation', $par_)) $par->fragmentation = $par_['fragmentation'];

  // Set structure file
  $par->log_file        = "$data_path/exec/$id/log.txt";

  $compounds = explode("\n", $par_['compound']);
  $tasks = [];
  for ($icompound = 0; $icompound < count($compounds); $icompound++) {
    $compound = $compounds[$icompound];

    if (trim($compound) != "") {
      $task = new Task;

      echo "Set query structure $icompound...\n";
      $task->smi = $compound;

      echo "Set working directory $icompound...\n";
      $work = "$data_path/exec/$id/work/$icompound";
      $cmd = "if [ -d $work ]; then rm -rf $work; fi; mkdir -p $work";
      shell_exec($cmd);
      $task->work = $work;

//      $cmd = "echo '$compound' >$work/input.smi; $babel_path $work/input.smi $work/input.sdf --gen3D";
//      echo "$cmd\n";
//      shell_exec($cmd);

      $tasks[] = $task;
    }
  }

  return $tasks;
}

function drift($id, $email) {
  global $babel_path, $data_path;

  $par = new Par;

  echo "\nSet inputs ...\n";
  set_inputs($email, $id);

  echo "\nInitialize working folder ...\n";
  $tasks = init_tasks($par, $id, $email);

  foreach ($tasks as $icompound => $task) {
    echo "\nSearch compounds by FP2 ...\n";
    fp2_search($par, $task);

    if (isset($par->search_pharm) && $par->search_pharm) {
      echo "\nSearch compounds by pharmacophore...\n";
      pharm_search($par, $task);
    }

    $ncompounds = count(array_keys($task->compounds));
    echo "$ncompounds Compounds found!\n";

    echo "\nFragment compounds ...\n";
    if ($par->fragmentation) {
      fragment_compounds($par, $task);
    } else {
      $task->fragmentation = ['compounds'=>[], 'fragments'=>[]];
    }

    echo "\nFind Targets and Assays ...\n";
    find_targets_assays($par, $task);
    $ntargets = count(array_keys($task->targets));
    echo "$ntargets Targets found!\n";

    echo "\nFind Functions and Processes ...\n";
    find_functions_processes_v2($par, $task);

    echo "\nRank targets ...\n";
    rank_targets($par, $task);

    echo "Update result ...\n";
    insert_result($id, $icompound, json_encode($task));
  }

  echo "Update task ...\n";
  update_task($id, ["status"=>"finished"], ["tfinish"=>"CURRENT_TIMESTAMP()"]);

  echo "Send emails to the user ...\n";
//    send_result_mail($id, $email, $host, true);
}

drift($id, $email);
