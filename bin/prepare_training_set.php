<?php

ini_set('memory_limit', '1024M'); // !!! IMPORTANT

require_once(__DIR__.'/mail_utils.php');
require_once(__DIR__.'/config.php');
require_once(__DIR__.'/chembl_utils.php');

use Spatie\Async\Pool;

$query = "SELECT td.chembl_id as target, cs.sequence, md.chembl_id as compound, ct.canonical_smiles as smiles, act.standard_value as kd FROM (SELECT assay_id, molregno, standard_value from activities act where standard_type='Kd' and CONCAT('', standard_value*1)=standard_value and standard_units='nM') act INNER JOIN assays a ON act.assay_id = a.assay_id INNER JOIN molecule_dictionary md ON md.molregno=act.molregno INNER JOIN target_dictionary td ON td.tid=a.tid INNER JOIN target_components tc ON td.tid=tc.tid INNER JOIN component_sequences cs ON cs.component_id=tc.component_id INNER JOIN compound_structures ct ON ct.molregno=act.molregno";
#$query = "SELECT td.chembl_id as target, cs.sequence, md.chembl_id as compound, ct.canonical_smiles as smiles, act.standard_value as kd FROM (SELECT assay_id, molregno, standard_value from activities act where standard_type='Kd' and CONCAT('', standard_value*1)=standard_value and standard_units='nM' ORDER BY RAND() LIMIT 5000) act INNER JOIN assays a ON act.assay_id = a.assay_id INNER JOIN molecule_dictionary md ON md.molregno=act.molregno INNER JOIN target_dictionary td ON td.tid=a.tid INNER JOIN target_components tc ON td.tid=tc.tid INNER JOIN component_sequences cs ON cs.component_id=tc.component_id INNER JOIN compound_structures ct ON ct.molregno=act.molregno";

$rows = chembl_fetch($query);
foreach ($rows as $row) {
  echo sprintf("%s %s %s %s %s\n", $row['target'], $row['sequence'], $row['compound'], $row['smiles'], $row['kd']);
}
