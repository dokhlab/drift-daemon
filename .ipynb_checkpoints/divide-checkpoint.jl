function main()
  inputfile = ARGS[1]
  outprefix = ARGS[2]
  k = parse(Int, ARGS[3])

  ifile = open(inputfile)
  n = 0
  for line in eachline(inputfile)
    if startswith(line, "\$\$\$\$")
      n += 1
    end
  end
  println(n)
  ni = div(n, k)
  println(ni)
  i = 1
  isdf = 1
  outfile = string(outprefix, isdf, ".sdf")
  ofile = open(outfile, "w+")
  for line in eachline(inputfile)
    println(ofile, line)
    if strip(line) == "\$\$\$\$"
      if i % 100000 == 0
        println(isdf, "-", i)
      end

      i += 1
      if isdf < k && i > ni
        i = 1
        isdf += 1
        close(ofile)
        outfile = string(outprefix, isdf, ".sdf")
        ofile = open(outfile, "w+")
      end
    end
  end
  close(ofile)
  close(ifile)
end

main()
