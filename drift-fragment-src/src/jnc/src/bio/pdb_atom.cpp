#include "pdb_writer.hpp"
#include "pdb_atom.hpp"

namespace jnc {

namespace pdb {

int Atom::get_element_type() const {
    if (element == "X") {
        return ::jnc::pdb::get_atom_element(name);
    }
    else {
        return ::jnc::get_atom_element(element);
    }
}

std::ostream &operator <<(std::ostream &output, const Atom &atom) {
    PdbWriter(output).write_atom(atom);
    return output;
}

}

} // namespace jnc

