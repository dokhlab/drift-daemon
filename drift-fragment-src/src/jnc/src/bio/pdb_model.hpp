#pragma once

#include "pdb_chain.hpp"

namespace jnc {

namespace pdb {

class Model : public std::vector<Chain> {
public:
    std::string name;
    int num;

    std::vector<const Atom *> patoms() const;

    std::vector<Atom *> patoms();

    std::vector<const Residue *> presidues() const;

    std::vector<Residue *> presidues();
};

} // namespace pdb

} // namespace jnc

