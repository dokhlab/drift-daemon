#pragma once

#include "pdb_model.hpp"
#include "pdb_types.hpp"
#include "../core/string.hpp"

namespace jnc {

namespace pdb {

class Pdb : public V<Model> {
public:
    std::string name;

    Pdb() {}

    Pdb(const std::string &filename);

    void read(const std::string &filename);

    void remove_hydrogens();

    void sort();
};

/**
 * Write a ine in a PDB file.
 */
inline void write_line(
    std::ostream &out,
    double x, double y, double z,
    const std::string &atom_name, int atom_index,
    const std::string &residue_name, int residue_index, const std::string &chain_name)
{
    out
        << string_format("ATOM%7d  %-4.4s%3.3s%2.2s%4d%12.3f%8.3f%8.3f%6.2f%6.2f%12.12s  ",
                         atom_index + 1, atom_name, residue_name, chain_name, residue_index + 1, x, y, z, 1.00, 0.00, "" + std::string(1, atom_name[0]))
        << std::endl;
}

std::ostream &operator <<(std::ostream &output, const Pdb &pdb);

} // namespace pdb

} // namespace jnc

