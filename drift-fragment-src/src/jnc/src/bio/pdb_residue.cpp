#include "pdb_writer.hpp"
#include "pdb_residue.hpp"

namespace jnc {

namespace pdb {

Atom &Residue::get_atom(std::string name) {
    for (auto &&atom : *this) {
        if (atom.name == name) return atom;
    }
    std::cerr << *this << std::endl;
    JN_DIE("Atom '" + name + "' not found");
}

const Atom &Residue::get_atom(std::string name) const {
    for (auto &&atom : *this) {
        if (atom.name == name) return atom;
    }
    std::cerr << *this << std::endl;
    JN_DIE("Atom '" + name + "' not found");
}

std::ostream &operator <<(std::ostream &output, const Residue &residue) {
    PdbWriter(output).write_residue(residue);
    return output;
}

}

} // namespace jnc

