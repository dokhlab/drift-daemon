#pragma once

#include <vector>
#include "pdb_atom.hpp"

namespace jnc {

namespace pdb {

class Residue : public std::vector<Atom> {
public:
    std::string name;
    int num = -1;
    bool is_std = true;

    Atom &get_atom(std::string name);

    const Atom &get_atom(std::string name) const;

};

std::ostream &operator <<(std::ostream &output, const Residue &residue);

}

} // namespace jnc

