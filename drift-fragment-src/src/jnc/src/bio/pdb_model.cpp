#include "pdb_writer.hpp"
#include "pdb_model.hpp"

namespace jnc {

namespace pdb {

V<const Atom *> Model::patoms() const {
    V<const Atom *> as;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            for (auto && atom : res) {
                as.push_back(&atom);
            }
        }
    }
    return std::move(as);
}

V<Atom *> Model::patoms() {
    V<Atom *> as;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            for (auto && atom : res) {
                as.push_back(&atom);
            }
        }
    }
    return std::move(as);
}

V<const Residue *> Model::presidues() const {
    V<const Residue *> rs;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            rs.push_back(&res);
        }
    }
    return std::move(rs);
}

V<Residue *> Model::presidues() {
    V<Residue *> rs;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            rs.push_back(&res);
        }
    }
    return std::move(rs);
}

std::ostream &operator <<(std::ostream &output, const Model &model) {
    PdbWriter l(output);
    l.write_model(model);
    l.write_file_end();
    return output;
}

} // namespace pdb

} // namespace jnc

