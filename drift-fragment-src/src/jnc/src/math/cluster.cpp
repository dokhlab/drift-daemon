#include "cluster.hpp"
#include "cluster_kmeans.hpp"

namespace jnc {

std::vector<int> kmeans(const Mat &m, int k) {
    Kmeans kmeans;
    kmeans.k = k;
    kmeans.run(m);

    std::vector<int> v;
    for (auto && cluster : kmeans.clusters) {
        v.push_back(cluster[0]);
    }

    return std::move(v);
}

}

