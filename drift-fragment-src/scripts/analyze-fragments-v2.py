import sys
import os
import glob
import subprocess
import re
import numpy as np
import json
import openbabel

def obconvert(istring, iformat, oformat):
  obConversion = openbabel.OBConversion()
  obConversion.SetInAndOutFormats(iformat, oformat)

  mol = openbabel.OBMol()
  obConversion.ReadString(mol, istring)
  return obConversion.WriteString(mol)

list_file = sys.argv[1]
out_file = sys.argv[2]

#fragment_files = {}

compound2nfragments = []
fragment2compound = []

compound_names = []
fragment_names = []

compound_id = 0
for line in open(list_file):
    compound = line.strip()
    if compound != '':
        prefix, ext = os.path.splitext(compound)

        if ext != '.mol2':
            cmd = 'babel {0} {1}-d.mol2 --gen3d -d'.format(compound, prefix)
            print('Converting {0} to {1}.mol2...'.format(compound, prefix))
            output = subprocess.check_output(cmd, shell=True)
        else:
            cmd = 'babel {0} {1}-d.mol2 -d'.format(compound, prefix)
            print('Converting {0} to {1}.mol2...'.format(compound, prefix))
            output = subprocess.check_output(cmd, shell=True)

        compound = prefix + '-d.mol2'
        compound_names.append(compound)

        cmd = 'fragment {0} -p c{1:04d}'.format(compound, compound_id + 1)
        output = subprocess.check_output(cmd, shell=True)

        fragments = glob.glob('c{0:04d}-f*.mol2'.format(compound_id + 1))
        fragment_names.extend(fragments)

        for i in range(len(fragments)):
            fragment2compound.append(compound_id)
#        fragment_files[compound] = fragments

        compound2nfragments.append(len(fragments))

        compound_id += 1

#print(fragment_files)

f = open('fragments.txt', 'w+')
f.write('\n'.join(fragment_names))
f.close()

cmd = 'compare fragments.txt scores.txt'
output = subprocess.check_output(cmd, shell=True)

scores = np.genfromtxt('scores.txt');
# print('scores', len(scores), len(scores[0]), scores)

unique_id = -1
real2unique = []
real_id = []
for i in range(len(fragment_names)):
    unique = True
    for j in range(0, i):
        if scores[i][j] == 1:
            unique = False
            real2unique.append(real2unique[j])
            real_id[real2unique[j]].append(i)
            break
    if unique:
        real_id.append([i])
        unique_id += 1
        real2unique.append(unique_id)
n_uniques = unique_id + 1

unique2frequency = [0 for i in range(n_uniques)]
n = 0
for i in range(len(compound_names)):
    unique_id_set = set()
    for j in range(compound2nfragments[i]):
        unique_id = real2unique[n]
        unique_id_set.add(unique_id)
        n += 1
    for j in unique_id_set:
        unique2frequency[j] += 1

#of = open(out_file, 'w+')
#for i in range(n_uniques):
#    of.write('Fragment {0} (frequency: {1}) {2}\n'.format(i + 1, unique2frequency[i], ' '.join(fragment_names[j] for j in real_id[i])))
#of.close()

def file_get_contents(filename):
    with open(filename) as f:
        return f.read()

result = {}
result['compounds'] = []
result['fragments'] = []

for i in range(len(compound_names)):
    compound = compound_names[i]
    name, ext = os.path.splitext(compound)
    name = os.path.basename(name)

    cmd = 'babel {} tmp.smi'.format(compound)
    output = subprocess.check_output(cmd, shell=True)

    result['compounds'].append({
        'name': name,
#        'file': file_get_contents(compound),
        'smi': re.split('\s+', file_get_contents('tmp.smi').strip())[0]
    })

for unique_id in range(n_uniques):
    real_ids = real_id[unique_id]

    source = [{
        'compound': fragment2compound[frag_id],
        'index': [int(re.split('\s+', line.strip())[0]) for line in file_get_contents(os.path.splitext(fragment_names[frag_id])[0] + '.txt').split('\n') if line.strip() != '']
    } for frag_id in real_ids]

    smi_file = os.path.splitext(fragment_names[real_ids[0]])[0] + '.smi'
    cmd = "babel {0} {1}; perl -i -lane '$F[0]=~s/U/\*/g;print $F[0]' {1}".format(fragment_names[real_ids[0]], smi_file)
    output = subprocess.check_output(cmd, shell=True)

#    svg_file = os.path.splitext(fragment_names[real_ids[0]])[0] + '.svg'
#    cmd = 'obabel {} -O {}'.format(smi_file, svg_file)
#    output = subprocess.check_output(cmd, shell=True)

    result['fragments'].append({
        'smi': re.split('\s+', file_get_contents(smi_file).strip())[0],
#        'svg': file_get_contents(svg_file).strip(),
        'count': unique2frequency[unique_id],
        'source': source
    })

with open(out_file, 'w+') as ofile:
    json.dump(result, ofile)

