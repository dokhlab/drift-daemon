#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <functional>

struct Graph {
    std::vector<int> nodes;
    jnc::Mati edges;

    Graph(const jnc::mol2::Mol2 &m) {
        int n = m.atoms.size();

        nodes.resize(n);
        for (int i = 0; i < n; i++) {
            nodes[i] = int(std::hash<std::string>{}(m.atoms[i].type));
        }

        edges = jnc::Mati::Zero(n, n);
        for (auto && b : m.bonds) {
            int i = b.origin_atom_id;
            int j = b.target_atom_id;
            int t = int(std::hash<std::string>{}(b.type));
            edges(i, j) = edges(j, i) = t;
        }
    }
};

struct SubStructure {
    const Graph &g1;
    const Graph &g2;

    SubStructure(const Graph &g1_, const Graph &g2_) :
        g1(g1_), g2(g2_)
    {}

    bool check() {
        std::vector<int> match(g1.nodes.size(), -1);
        std::vector<bool> assign(g2.nodes.size(), false);
        return find_match(match, assign, 0);
    }

    bool find_match(std::vector<int> &match, std::vector<bool> &assign, int p) {
        for (int i = 0; i < g2.nodes.size(); i++) {
            if (!assign[i]) {
                if (g1.nodes[p] == g2.nodes[i]) {
                    bool flag = true;
                    for (int j = 0; j < p; j++) {
                        if (g1.edges(j, p) != g2.edges(match[j], i)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        match[p] = i;
                        assign[i] = true;
                        if (p + 1 == match.size()) {
                            return true;
                        } else if (find_match(match, assign, p + 1)) {
                            return true;
                        } else {
                            assign[i] = false;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
        return false;
    }
};

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string list_file = opt.argv[1];
    std::string out_file = opt.argv[2];

    std::deque<Graph> graphs;

    std::string line;
    std::ifstream ifile(list_file.c_str());
    while (ifile >> line) {
        jnc::mol2::Mol2 structure(line);
        graphs.push_back(structure);
    }
    ifile.close();

    int n = graphs.size();

    jnc::Mati m = jnc::Mati::Zero(n, n);
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            SubStructure sub(graphs[i], graphs[j]);
            if (graphs[i].nodes.size() == graphs[j].nodes.size() && sub.check()) {
                m(i, j) = m(j, i) = 1;
            }
        }
    }

    std::ofstream ofile(out_file.c_str());
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            ofile << m(i, j) << ' ';
        }
        ofile << std::endl;
    }
    ofile.close();

    return 0;
}

