#include <iostream>
#include <jnc/bio>

using Mol2 = jnc::mol2::Mol2;

using Indices = std::vector<int>;

using Atom = jnc::mol2::Atom;
using AtomStore = std::deque<Atom>;

struct Bond {
    Indices atoms;
    int order = 1;
    bool rotatable = true;
};
using BondStore = std::deque<Bond>;

using BondMap = std::map<int, std::map<int, int>>;
using AtomBondMap = std::map<int, Indices>;

struct Cycle {
    Indices atoms;
    Indices bonds;
};
using CycleStore = std::deque<Cycle>;

struct Fragment {
    Indices atoms;
    Indices rbonds; // number of rotatable bonds of each atom
};
using FragmentStore = std::deque<Fragment>;

Mol2 mol2_sub(const Mol2 &mol2_old, const Fragment &frag) {
    auto &atoms = frag.atoms;
    auto &rbonds = frag.rbonds;

    int n = mol2_old.atoms.size();

    Mol2 mol2_new;

    // Set index map
    std::vector<int> m(n, -1);
    for (int i = 0, j = 0; i < n; i++) {
        auto it = std::find(atoms.begin(), atoms.end(), i);
        if (it == atoms.end()) {
            continue;
        } else {
            m[i] = j;
            j++;
        }
    }

    // Set atoms
    for (int i = 0; i < n; i++) {
        if (m[i] != -1) {
            mol2_new.atoms.push_back(mol2_old.atoms[i]);
        }
    }

    // Set bonds
    for (auto && bond : mol2_old.bonds) {
        auto b = bond;
        b.origin_atom_id = m[bond.origin_atom_id];
        b.target_atom_id = m[bond.target_atom_id];
        if (b.origin_atom_id != -1 && b.target_atom_id != -1) mol2_new.bonds.push_back(std::move(b));
    }

    // Extra
    int k = atoms.size();
    for (int i = 0; i < atoms.size(); i++) {
        for (int j = 0; j < rbonds[i]; j++) {
            mol2_new.atoms.push_back(jnc::mol2::Atom {"U", 0, 0, 0, "U"});
            mol2_new.bonds.push_back(jnc::mol2::Bond {i, k, "1"});
            k++;
        }
    }

    mol2_new.num_atoms = mol2_new.atoms.size();
    mol2_new.num_bonds = mol2_new.bonds.size();

    return std::move(mol2_new);
}

void read_mol2(const jnc::mol2::Mol2 &structure, AtomStore &atom_store, BondStore &bond_store) {
    int n_atoms = structure.atoms.size();
//    std::map<int, int> id_map;
    for (int i = 0; i < n_atoms; i++) {
//        std::cout << "Atom " << structure.atoms[i].subst_id << ' ' << structure.atoms[i].name << std::endl;
//        id_map[structure.atoms[i].subst_id] = i;
        atom_store.push_back(std::move(structure.atoms[i]));
    }

    for (auto && b : structure.bonds) {
//        std::cout << "Bond: " << b.origin_atom_id << ' ' << b.target_atom_id << std::endl;
        Bond bond;
//        bond.atoms.push_back(id_map[b.origin_atom_id]);
//        bond.atoms.push_back(id_map[b.target_atom_id]);
        if (b.type == "1") {
            bond.order = 1;
        } else {
            bond.order = 2;
        }

        bond.atoms.push_back(b.origin_atom_id);
        bond.atoms.push_back(b.target_atom_id);
        bond_store.push_back(std::move(bond));
    }
}

void set_bond_orders(BondStore &bond_store) {
}

int bonded_atom(const BondStore &bond_store, int atom, int bond) {
    if (bond_store.at(bond).atoms[0] == atom) {
        return bond_store.at(bond).atoms[1];
    } else {
        return bond_store.at(bond).atoms[0];
    }
}

class CyclesIdentifier {
public:
    std::vector<bool> bond_visited;
    std::vector<bool> atom_visited;

    const AtomStore &atom_store;
    const BondStore &bond_store;
    const BondMap &bond_map;
    const AtomBondMap &atom_bond_map;

    CyclesIdentifier(const AtomStore &atom_store_, const BondStore &bond_store_, const BondMap &bond_map_, const AtomBondMap &atom_bond_map_) :
        atom_store(atom_store_), bond_store(bond_store_), bond_map(bond_map_), atom_bond_map(atom_bond_map_)
    {}

    int get_cycle_from_path(const Indices &path, int atom, CycleStore &store) {
        Cycle cycle;
        auto it = std::find(path.begin(), path.end(), atom);
        for (auto p = it; p != path.end(); p++) {
            cycle.atoms.push_back(*p);
            auto q = std::next(p);
            if (q == path.end()) {
                cycle.bonds.push_back(bond_map.at(*p).at(atom));
            } else {
                cycle.bonds.push_back(bond_map.at(*p).at(*q));
            }
        }
        store.push_back(std::move(cycle));
        return store.size() - 1;
    }

    CycleStore identify() {
        CycleStore store;
        Indices path;

        bond_visited.resize(bond_store.size(), false);
        atom_visited.resize(atom_store.size(), false);

        for (int i = 0; i < atom_store.size(); i++) {
            identify_cycles(0, path, store);
        }

        return std::move(store);
    }

    void identify_cycles(int root, Indices &path, CycleStore &store) {
        if (!atom_visited[root]) {
            path.push_back(root);
            atom_visited[root] = true;
            if (atom_bond_map.count(root)) {
                for (auto && bond : atom_bond_map.at(root)) {
                    if (!bond_visited[bond]) {
                        bond_visited[bond] = true;
                        int neighbor = bonded_atom(bond_store, root, bond);
                        if (!atom_visited[neighbor]) {
                            identify_cycles(neighbor, path, store);
                        } else {
                            get_cycle_from_path(path, neighbor, store);
                        }
                    }
                }
            }
            path.pop_back();
        }
    }
};

/*
class Fragmenter {
public:
    Indices &cycles;

    Fragmenter() {
    }

    FragmentStore fragment() {
        FragmentStore store;

        Indices high_order_bonds;
        for (int i = 0; i < bond_store.size(); i++) {
            if (bond_store[i].order > 1) {
                high_order_bonds.push_back(i);
            }
        }

        for (int i = 0; i < cycles.size(); i++) {
            int cycle = cycles[i];
            if (!cycle_visited[cycle]) {
                for (int j = i + 1; j < cycles.size(); j++) {
                }
                for (int j = 0; j < high_order_bonds.size(); j++) {
                }
            }
        }

        return std::move(store);
    }
};
*/

Indices get_leaves(const AtomStore &atom_store, const AtomBondMap &atom_bond_map) {
    Indices inds;
    for (int i = 0; i < atom_store.size(); i++) {
        if (atom_bond_map.count(i)) {
            if (atom_bond_map.at(i).size() == 1) {
                inds.push_back(i);
            }
        }
    }
    return std::move(inds);
}

void set_rotatable_bonds(const AtomStore &atom_store, BondStore &bond_store, const CycleStore &cycle_store, const AtomBondMap &atom_bond_map) {
    for (auto && cycle : cycle_store) {
        for (auto && bond_id : cycle.bonds) {
            auto &bond = bond_store[bond_id];
            bond.rotatable = false;
        }
    }
    for (auto && bond : bond_store) {
        if (bond.order > 1) {
            bond.rotatable = false;
        }
    }

    auto leaves = get_leaves(atom_store, atom_bond_map);

    for (auto && leaf : leaves) {
        if (atom_bond_map.count(leaf)) {
            for (auto && bond : atom_bond_map.at(leaf)) {
                bond_store[bond].rotatable = false;
            }
        }
    }
}

class Fragmenter {
public:
    std::vector<bool> atom_visited;
    std::vector<bool> bond_visited;

    const AtomStore &atom_store;
    const BondStore &bond_store;
    const AtomBondMap &atom_bond_map;

    Fragmenter(const AtomStore &atom_store_, const BondStore &bond_store_, const AtomBondMap &atom_bond_map_) :
        atom_store(atom_store_), bond_store(bond_store_), atom_bond_map(atom_bond_map_)
    {}

    FragmentStore fragment() {
        atom_visited.resize(atom_store.size(), false);
        bond_visited.resize(bond_store.size(), false);

        FragmentStore store;

        for (int i = 0; i < atom_store.size(); i++) {
            if (!atom_visited[i]) {
                store.push_back(Fragment{});
                add_fragment(i, store.back());
            }
        }

        return std::move(store);
    }

    void add_fragment(int root, Fragment &fragment) {
        atom_visited[root] = true;
        fragment.atoms.push_back(root);
        fragment.rbonds.push_back(0);
        if (atom_bond_map.count(root)) {
            for (auto && bond : atom_bond_map.at(root)) {
                if (!bond_visited[bond]) {
                    if (!bond_store[bond].rotatable) {
                        bond_visited[bond] = true;
                        int neighbor = bonded_atom(bond_store, root, bond);
                        if (!atom_visited[neighbor]) {
                            add_fragment(neighbor, fragment);
                        }
                    } else {
                        fragment.rbonds.back()++;
                    }
                }
            }
        }
    }
};

AtomBondMap get_atom_bond_map(const BondStore &bond_store) {
    AtomBondMap atom_bond_map;
    for (int i = 0; i < bond_store.size(); i++) {
        auto &bond = bond_store[i];
        atom_bond_map[bond.atoms[0]].push_back(i);
        atom_bond_map[bond.atoms[1]].push_back(i);
    }
    return std::move(atom_bond_map);
}

BondMap get_bond_map(const BondStore &bond_store) {
    BondMap bond_map;
    for (int i = 0; i < bond_store.size(); i++) {
        auto &bond = bond_store[i];
        int a = bond.atoms[0];
        int b = bond.atoms[1];
        bond_map[a][b] = i;
        bond_map[b][a] = i;
    }
    return std::move(bond_map);
}

int main(int argc, char **argv) {
    using namespace std;

    jnc::Opt opt(argc, argv);

    std::string mol2_file = opt.argv[1];
    auto prefix = jnc::path_splitext(mol2_file)[0];
    if (opt.has("p", "prefix")) {
        opt.set(prefix, "p", "prefix");
    }

    jnc::mol2::Mol2 structure(mol2_file);

    AtomStore atom_store;
    BondStore bond_store;
    read_mol2(structure, atom_store, bond_store);

    auto bond_map = get_bond_map(bond_store);
    auto atom_bond_map = get_atom_bond_map(bond_store);

    set_bond_orders(bond_store);

    CyclesIdentifier cycles_identifier(atom_store, bond_store, bond_map, atom_bond_map);
    auto cycle_store = cycles_identifier.identify();

    set_rotatable_bonds(atom_store, bond_store, cycle_store, atom_bond_map);

    Fragmenter fragmenter(atom_store, bond_store, atom_bond_map);
    auto fragment_store = fragmenter.fragment();

    // Print fragment store
    for (int i = 0; i < fragment_store.size(); i++) {
        auto out_file = jnc::string_format("%s-f%04d.txt", prefix, i + 1);

        std::ofstream ofile(out_file.c_str());
//        ofile << "Fragment " << i + 1 << ":";
        for (auto && atom : fragment_store[i].atoms) {
            ofile << atom + 1 << ' ' << atom_store[atom].name << std::endl;
        }
        ofile << std::endl;
        ofile.close();

        out_file = jnc::string_format("%s-f%04d.mol2", prefix, i + 1);
        std::cout << "Writing to mol2 file: " << out_file << std::endl;
        auto frag = mol2_sub(structure, fragment_store[i]);
        frag.write(out_file);
        std::cout << std::endl;
    }

    return 0;
}

