import sys

inputfile = sys.argv[1]
outprefix = sys.argv[2]

i = 1
ofilename = "{}-{}.sdf".format(outprefix, i)
ofile = open(ofilename, "w+")
for line in open(inputfile):
  ofile.write(line)
  if line.strip() == '$$$$':
    ofile.close()
    i += 1
    ofilename = "{}-{}.sdf".format(outprefix, i)
    ofile = open(ofilename, "w+")

ofile.close()
