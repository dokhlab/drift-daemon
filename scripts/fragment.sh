#!/bin/bash -l

in_file=$1
out_file=$2

wd=$(cd $(dirname $0); pwd)

prefix=${wd}/../drift-fragment

export PATH=${prefix}/bin:$PATH

python3 ${prefix}/scripts/analyze-fragments.py $in_file $out_file

rm c*-f*.*
