from rdkit import Chem
from rdkit.Chem import AllChem
import sys

ifilename = sys.argv[1]
ofilename = sys.argv[2]

suppl = Chem.SDMolSupplier(ifilename)
w = Chem.SDWriter(ofilename)

for (im, m) in enumerate(suppl):
  if m:
    name = m.GetProp("_Name")
    if im % 1000 == 0:
      print(im, name)
    m2=Chem.RemoveHs(m)
    n = m2.GetNumAtoms()
    if n < 50:
      w.write(m2)

