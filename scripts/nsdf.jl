function main()
  inputfile = ARGS[1]

  ifile = open(inputfile)
  n = 0
  for line in eachline(inputfile)
    if startswith(line, "\$\$\$\$")
      n += 1
    end
  end
  println(n)
  close(ifile)
end

main()
