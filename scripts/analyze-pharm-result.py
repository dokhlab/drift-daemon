import sys
import re

pharm_result_file = sys.argv[1]
outfile = sys.argv[2]

stat = {}
for line in open(pharm_result_file):
  l = re.split(',', line.strip())
  if len(l) == 7:
    compoundid = l[4]
    if not compoundid in stat:
      stat[compoundid] = 0
    stat[compoundid] += 1

compounds = stat.keys()
compounds = sorted(compounds, key=lambda compoundid: -stat[compoundid])

f = open(outfile, 'w+')
for compoundid in compounds:
  f.write('{0} {1}\n'.format(compoundid, stat[compoundid]))
f.close()
