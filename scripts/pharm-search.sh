smifile=$1
lib=$2
outfile=$3

wd=$(cd $(dirname $0); pwd)

export LC_ALL=C

name=pharmsearch

echo create sdf file and sample conformers for the smi file ...
rm -f ${name}.sdf
python3 ${wd}/smi2sdf.py $smifile ${name}.sdf 5

echo Split the input sdf file ...
rm -f ${name}-*.sdf
python3 ${wd}/splitsdf.py ${name}.sdf ${name}

echo Delete empty sdf files ...
for sdf in $(ls ${name}-*.sdf); do
  size=$(du -s ${sdf} | perl -lane 'print $F[0]')
  if [[ $size -eq 0 ]]; then
    rm ${sdf}
  fi
done

echo Generate features for input files
rm -f *${name}*.json
for conf in $(ls ${name}-*.sdf); do
  confname=${conf%%.*}
  ${wd}/../Pharmer/pharmer.static pharma -in ${confname}.sdf -out ${confname}.json
  python3 ${wd}/prepare_features.py ${confname}.json query-${confname} 5
done

echo Search the database ...
for jsonfile in $(ls query-*.json); do
  echo ${jsonfile}
  ${wd}/../Pharmer/pharmer.static dbsearch -dbdir ${lib} -in ${jsonfile} -extra-info
done >${name}-result.txt

echo Get the statistics of the result ...
python3 ${wd}/analyze-pharm-result.py ${name}-result.txt ${outfile}
