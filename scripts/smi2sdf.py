from rdkit import Chem
from rdkit.Chem import AllChem
import sys

def gen_conformers(mol, numConfs=5, maxAttempts=100, pruneRmsThresh=0.1, useExpTorsionAnglePrefs=True, useBasicKnowledge=True, enforceChirality=True):
  ids = AllChem.EmbedMultipleConfs(mol, numConfs=numConfs, maxAttempts=maxAttempts, pruneRmsThresh=pruneRmsThresh, useExpTorsionAnglePrefs=useExpTorsionAnglePrefs, useBasicKnowledge=useBasicKnowledge, enforceChirality=enforceChirality, numThreads=0)
  return list(ids)

smifile = sys.argv[1]
sdffile = sys.argv[2]
nConfs = int(sys.argv[3])

f = open(smifile)
smi = f.read().strip().split('/\s+/')[0]
f.close()

m = Chem.MolFromSmiles(smi)
m2=Chem.AddHs(m)
confIds = gen_conformers(m2, nConfs)
#AllChem.EmbedMolecule(m2)
#AllChem.MMFFOptimizeMolecule(m2)

w = Chem.SDWriter(sdffile)
for confId in confIds:
  w.write(m2, confId=confId)
w.close()
