import json
import sys
import math
from itertools import combinations
import random

def file_get_contents(filename):
  with open(filename) as f:
    return f.read()

def file_put_contents(filename, content):
  with open(filename, 'w+') as f:
    f.write(content)

def dist(p1, p2):
  dx = p1['x'] - p2['x']
  dy = p1['y'] - p2['y']
  dz = p1['z'] - p2['z']
  return math.sqrt(dx*dx+dy*dy+dz*dz)

def issparse(points):
  for p1, p2 in list(combinations(points, 2)):
    d = dist(p1, p2)
    if d < 2:
      return False
  return True

inputfile = sys.argv[1]
outprefix = sys.argv[2]
k = int(sys.argv[3])

intact_features = json.loads(file_get_contents(inputfile))

points = intact_features['points']

npts = len(points)

i = 0
while True:
  random.shuffle(points)
  c = points[:4]
  if issparse(c):
    features = {}
    features['points'] = list(c)
    filename = "{0}-{1}.json".format(outprefix, i+1)
    file_put_contents(filename, json.dumps(features))
    i += 1
    if i >= k:
        break
