import sys
from rdkit import Chem
from rdkit.Chem import AllChem, TorsionFingerprints
from rdkit.ML.Cluster import Butina
import argparse

def gen_conformers(mol, numConfs=100, maxAttempts=1000, pruneRmsThresh=0.1, useExpTorsionAnglePrefs=True, useBasicKnowledge=True, enforceChirality=True):
  ids = AllChem.EmbedMultipleConfs(mol, numConfs=numConfs, maxAttempts=maxAttempts, pruneRmsThresh=pruneRmsThresh, useExpTorsionAnglePrefs=useExpTorsionAnglePrefs, useBasicKnowledge=useBasicKnowledge, enforceChirality=enforceChirality, numThreads=0)
  return list(ids)
  
def write_conformers_to_sdf(mol, w, rmsClusters, conformerPropsDict):
  name = mol.GetProp("_Name")
  for (icluster, cluster) in enumerate(rmsClusters):
    iname = "{}-{}".format(name, icluster + 1)
    confId = cluster[0]
    mol.SetProp("_Name", iname)
    w.write(mol, confId=confId)
  w.flush()
  
def calc_energy(mol, conformerId, minimizeIts):
  ff = AllChem.MMFFGetMoleculeForceField(mol, AllChem.MMFFGetMoleculeProperties(mol), confId=conformerId)
  ff.Initialize()
  ff.CalcEnergy()
  results = {}
  if minimizeIts > 0:
    results["converged"] = ff.Minimize(maxIts=minimizeIts)
  results["energy_abs"] = ff.CalcEnergy()
  return results
  
def cluster_conformers(mol, mode="RMSD", threshold=2.0):
  if mode == "TFD":
    dmat = TorsionFingerprints.GetTFDMatrix(mol)
  else:
    dmat = AllChem.GetConformerRMSMatrix(mol, prealigned=False)
  rms_clusters = Butina.ClusterData(dmat, mol.GetNumConformers(), threshold, isDistData=True, reordering=True)
  return rms_clusters
  
def align_conformers(mol, clust_ids):
  rmslist = []
  AllChem.AlignMolConformers(mol, confIds=clust_ids, RMSlist=rmslist)
  return rmslist

def parse_args():
  parser = argparse.ArgumentParser()
  parser.add_argument("ifilename")
  parser.add_argument("ofilename")
  parser.add_argument("--num-confs", "-n", help="Number of conformations", type=int, default=100)
  parser.add_argument("--max-attempts", "-m", help="Max number of attempts", type=int, default=1000)
  parser.add_argument("--prune-rms-thresh", "-p", help="Prune RMS Threshold", type=float, default=0.1)
  parser.add_argument("--cluster-method", "-c", help="Cluster Method (RMSD)", type=str, default="RMSD")
  parser.add_argument("--cluster-threshold", "-t", help="Cluster Threshold", type=float, default=1.0)
  parser.add_argument("--minimize-iterations", "-i", help="Number of Minimization Iterations", type=int, default=0)
  return parser.parse_args()

def rms_conformers(mol, threshold=1.5, num_confs=100, max_attempts=1000, prune_rms_thresh=0.1):
  m = Chem.AddHs(mol)
  conformerIds = gen_conformers(m, num_confs, max_attempts, prune_rms_thresh, True, True, True)
  rmsClusters = cluster_conformers(m, 'RMSD', threshold)
  return m, rmsClusters

if __name__ == '__main__':
  args = parse_args()

  suppl = Chem.ForwardSDMolSupplier(args.ifilename)
  w = Chem.SDWriter(args.ofilename)
  for mol in suppl:
    name = mol.GetProp("_Name")
    print("Molecule ", name)

    if mol is None: continue

    m = Chem.AddHs(mol)
    conformerIds = gen_conformers(m, args.num_confs, args.max_attempts, args.prune_rms_thresh, True, True, True)
    conformerPropsDict = {}
    for conformerId in conformerIds:
      conformerPropsDict[conformerId] = {}
    rmsClusters = cluster_conformers(m, args.cluster_method, args.cluster_threshold)

    print("  generated", len(conformerIds), "conformers and", len(rmsClusters), "clusters")
    rmsClustersPerCluster = []
    clusterNumber = 0
    for cluster in rmsClusters:
      clusterNumber = clusterNumber+1
      rmsWithinCluster = align_conformers(m, cluster)
      for conformerId in cluster:
        props = conformerPropsDict[conformerId]
        props["cluster_no"] = clusterNumber
        props["cluster_centroid"] = cluster[0] + 1
        idx = cluster.index(conformerId)
        if idx > 0:
          props["rms_to_centroid"] = rmsWithinCluster[idx-1]
        else:
          props["rms_to_centroid"] = 0.0

    write_conformers_to_sdf(m, w, rmsClusters, conformerPropsDict)

