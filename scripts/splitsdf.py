import sys

def split_sdf(inputfile, outprefix):
  i = 1
  ofilename = "{}-{}.sdf".format(outprefix, i)
  ofile = open(ofilename, "w+")
  for line in open(inputfile):
    ofile.write(line)
    if line.strip() == '$$$$':
      ofile.close()
      i += 1
      ofilename = "{}-{}.sdf".format(outprefix, i)
      ofile = open(ofilename, "w+")
  ofile.close()

if __name__ == '__main__':
  split_sdf(sys.argv[1], sys.argv[2])
