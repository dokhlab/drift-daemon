import sys
import numpy as np
import os
import argparse
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Input, Dense, Dropout, BatchNormalization, Activation, Embedding, Lambda, Concatenate, Convolution1D, GlobalMaxPooling1D, SpatialDropout1D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2,l1
from sklearn.metrics import precision_recall_curve, auc, roc_curve

seq_rdic = ['A','I','L','V','F','W','Y','N','C','Q','M','S','T','D','E','R','H','K','G','P','O','U','X','B','Z']
seq_dic = {w: i+1 for i,w in enumerate(seq_rdic)}

def smi2fp(smi, drug_len):
  m = Chem.MolFromSmiles(smi)
  fp2 = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits=drug_len)
  return fp2.ToBitString()

def encode_aa(aa, seq_dic):
    if aa in seq_dic:
        return seq_dic[aa]
    else:
        return max(seq_dic.values()) + 1

def encode_seq(seq, seq_dic):
    if pd.isnull(seq):
        return [0]
    else:
        return [encode_aa(aa, seq_dic) for aa in seq]

def load_dataset(datasetfilename, prot_len, drug_len, with_label=True):
    data = pd.read_csv(datasetfilename)

    protein_feature = keras.preprocessing.sequence.pad_sequences(data["Sequence"].map(lambda seq: encode_seq(seq, seq_dic)), prot_len)
    drug_feature = np.stack(data["SMILES"].map(lambda smi: [int(i) for i in smi2fp(smi, drug_len)]))

    if with_label:
        label = data["Kd"].values
        label = np.stack(data["Kd"].map(lambda kd: 1 if float(kd) <= 100 else 0))
        return drug_feature, protein_feature, label
    else:
        return drug_feature, protein_feature

def PLayer(size, filters, activation, initializer, regularizer_param):
    def f(input):
        proteinmodel = Convolution1D(filters=filters, kernel_size=size, padding='same', kernel_initializer=initializer, kernel_regularizer=l2(regularizer_param))(input)
        proteinmodel = BatchNormalization()(proteinmodel)
        proteinmodel = Activation(activation)(proteinmodel)
        return GlobalMaxPooling1D()(proteinmodel)
    return f

def return_tuple(value):
    if type(value) is int:
        return [value]
    else:
        return tuple(value)

def create_model(args):
    regularizer_param = 0.001
    denseparams = {"kernel_initializer": args.initializer, "kernel_regularizer": l2(regularizer_param)}

    druginputs = Input(shape=(args.drug_len,))
    drugoutputs = druginputs
    for layer_size in return_tuple(args.drug_layers):
        drugoutputs = Dense(layer_size, **denseparams)(drugoutputs)
        drugoutputs = BatchNormalization()(drugoutputs)
        drugoutputs = Activation(args.activation)(drugoutputs)
        drugoutputs = Dropout(args.dropout)(drugoutputs)

    proteininputs = Input(shape=(args.prot_len,))
    proteinoutputs = Embedding(26,20, embeddings_initializer=args.initializer,embeddings_regularizer=l2(regularizer_param))(proteininputs)
    proteinoutputs = SpatialDropout1D(0.2)(proteinoutputs)
    proteinoutputs2 = [PLayer(stride_size, args.filters, args.activation, args.initializer, regularizer_param)(proteinoutputs) for stride_size in args.protein_strides]
    if len(proteinoutputs2)!=1:
        proteinoutputs = Concatenate(axis=1)(proteinoutputs2)
    else:
        proteinoutputs = proteinoutputs2[0]

    for protein_layer in return_tuple(args.protein_layers):
        proteinoutputs = Dense(protein_layer, **denseparams)(proteinoutputs)
        proteinoutputs = BatchNormalization()(proteinoutputs)
        proteinoutputs = Activation(args.activation)(proteinoutputs)
        proteinoutputs = Dropout(args.dropout)(proteinoutputs)

    outputs = Concatenate(axis=1)([drugoutputs, proteinoutputs])
    if args.fc_layers is not None:
        for fc_layer in return_tuple(args.fc_layers):
            outputs = Dense(units=fc_layer, **denseparams)(outputs)
            outputs = BatchNormalization()(outputs)
            outputs = Activation(args.activation)(outputs)
    outputs = Dense(1, activation='tanh', activity_regularizer=l2(regularizer_param), **denseparams)(outputs)
    outputs = Lambda(lambda x: (x+1.)/2.)(outputs)

    return keras.models.Model(inputs=[druginputs, proteininputs], outputs=outputs)

def parse_training_args():
    parser = argparse.ArgumentParser(description="DeepDrift")
    
    # training and validation set
    parser.add_argument("action", help="train | pred")
    parser.add_argument("datasetfile", help="Dataset File (Sequence,SMILES,Kd)")
    parser.add_argument("model", help="Model File")

    # test_params
    parser.add_argument("--with-label", "-W", help="Existence of label information in test DTI", action="store_true")
    # structure_params
    parser.add_argument("--protein-strides", '-w', help="Window sizes for model (only works for Convolution)", default=None, nargs="*", type=int)
    parser.add_argument("--protein-layers","-p", help="Dense layers for protein", default=None, nargs="*", type=int)
    parser.add_argument("--drug-layers", '-c', help="Dense layers for drugs", default=None, nargs="*", type=int)
    parser.add_argument("--fc-layers", '-f', help="Dense layers for concatenated layers of drug and target layer", default=None, nargs="*", type=int)
    # training_params
    parser.add_argument("--learning-rate", '-r', help="Learning late for training", default=1e-4, type=float)
    parser.add_argument("--n-epoch", '-e', help="The number of epochs for training or validation", type=int, default=10)
    # ptype
    parser.add_argument("--prot-len", "-l", help="Protein vector length", default=2500, type=int)
    parser.add_argument("--drug-len", "-L", help="Drug vector length", default=2048, type=int)
    # the other hyper-parameters
    parser.add_argument("--activation", "-a", help='Activation function of model', default='relu', type=str)
    parser.add_argument("--initializer", help='Activation function of model', default='glorot_normal', type=str)
    parser.add_argument("--dropout", "-D", help="Dropout ratio", default=0.2, type=float)
    parser.add_argument("--filters", "-F", help="Number of filters for convolution layer, only works for Convolution", default=64, type=int)
    parser.add_argument("--batch-size", "-b", help="Batch size", default=32, type=int)
    parser.add_argument("--decay", "-y", help="Learning rate decay", default=0.0, type=float)
    # output_params
    parser.add_argument("--output", "-o", help="Prediction output", type=str)

    return parser.parse_args()

def parse_pred_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("datasetfile", help="Dataset File (Sequence,SMILES)")
    parser.add_argument("model")
    parser.add_argument("--prot-len", "-l", help="Protein vector length", default=2500, type=int)
    parser.add_argument("--drug-len", "-L", help="Drug vector length", default=2048, type=int)
    parser.add_argument("--output-file", "-o", help="Output file", type=str)

    return parser.parse_args()

def parse_plot_args():
    parser = argparse.ArgumentParser(description="DeepDrift")
    
    # training and validation set
    parser.add_argument("action", help="train | pred")

    # test_params
    parser.add_argument("--with-label", "-W", help="Existence of label information in test DTI", action="store_true")
    # structure_params
    parser.add_argument("--protein-strides", '-w', help="Window sizes for model (only works for Convolution)", default=None, nargs="*", type=int)
    parser.add_argument("--protein-layers","-p", help="Dense layers for protein", default=None, nargs="*", type=int)
    parser.add_argument("--drug-layers", '-c', help="Dense layers for drugs", default=None, nargs="*", type=int)
    parser.add_argument("--fc-layers", '-f', help="Dense layers for concatenated layers of drug and target layer", default=None, nargs="*", type=int)
    # training_params
    parser.add_argument("--learning-rate", '-r', help="Learning late for training", default=1e-4, type=float)
    parser.add_argument("--n-epoch", '-e', help="The number of epochs for training or validation", type=int, default=10)
    # ptype
    parser.add_argument("--prot-len", "-l", help="Protein vector length", default=2500, type=int)
    parser.add_argument("--drug-len", "-L", help="Drug vector length", default=2048, type=int)
    # the other hyper-parameters
    parser.add_argument("--activation", "-a", help='Activation function of model', default='relu', type=str)
    parser.add_argument("--initializer", help='Activation function of model', default='glorot_normal', type=str)
    parser.add_argument("--dropout", "-D", help="Dropout ratio", default=0.2, type=float)
    parser.add_argument("--filters", "-F", help="Number of filters for convolution layer, only works for Convolution", default=64, type=int)
    parser.add_argument("--batch-size", "-b", help="Batch size", default=32, type=int)
    parser.add_argument("--decay", "-y", help="Learning rate decay", default=0.0, type=float)
    # output_params
    parser.add_argument("--output", "-o", help="Prediction output", type=str)

    return parser.parse_args()


def pred():
    args = parse_pred_args()
    model = tf.keras.models.load_model(args.model)
    drugs, proteins = load_dataset(args.datasetfile, args.prot_len, args.drug_len, with_label=False)
    prediction = model.predict(x=[drugs, proteins])

    f = open(args.output_file, 'w+')
    f.write('\n'.join(str(i[0]) for i in prediction))
    f.close()

def train():
    args = parse_training_args()
    if args.protein_strides==0:
        args.protein_strides = None

    drugs, proteins, labels = load_dataset(args.datasetfile, args.prot_len, args.drug_len)

    model = create_model(args)
    model.compile(optimizer=Adam(lr=args.learning_rate, decay=args.decay), loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()
    log_dir = "./logs"
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    model.fit(x=[drugs, proteins], y=labels, validation_split=0.2, epochs=args.n_epoch, batch_size=args.batch_size, shuffle=True, callbacks=[tensorboard_callback])

    model.save(args.model)

def plot():
    args = parse_plot_args()
    if args.protein_strides==0:
        args.protein_strides = None

    model = create_model(args)
    tf.keras.utils.plot_model(model, to_file='model.png', show_shapes=False, show_layer_names=True, rankdir='TB', expand_nested=False, dpi=300)

if __name__ == '__main__':
    if sys.argv[1] == 'train':
        train()
    elif sys.argv[1] == 'plot':
        plot()
    else:
        pred()

