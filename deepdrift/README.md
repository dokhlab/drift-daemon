# DeepDrift

## Requirement

    tensorflow >= 2.3.1
    pandas 
    scikit-learn  
    rdkit

## Usage 

1. Training

        deepdrift.py train <dataset file>
                 [--with-label WITH_LABEL]
                 [--window-sizes [WINDOW_SIZES [WINDOW_SIZES ...]]]
                 [--protein-layers [PROTEIN_LAYERS [PROTEIN_LAYERS ...]]]
                 [--drug-layers [DRUG_LAYERS [DRUG_LAYERS ...]]]
                 [--fc-layers [FC_LAYERS [FC_LAYERS ...]]]
                 [--learning-rate LEARNING_RATE] [--n-epoch N_EPOCH]
                 [--prot-vec PROT_VEC] [--prot-len PROT_LEN]
                 [--drug-vec DRUG_VEC] [--drug-len DRUG_LEN]
                 [--activation ACTIVATION] [--dropout DROPOUT]
                 [--filters N_FILTERS] [--batch-size BATCH_SIZE]
                 [--decay DECAY]
                 [--model SAVE_MODEL]

    Example:

        python3 deepdrift.py train  ./training_set_kd100.csv -W -c 512 128 -w 10 15 20 25 30 -p 128 -f 128 -r 0.0001 -l 2500 -V morgan_fp_r2 -L 2048 -D 0 -a elu -F 128 --batch-size 2 -y 0.0001 -o ./validation_output.csv -m ./model.model -e 100

This command will train a model for 100 epochs. The model will be saved in `./model.model`.

2. Prediction

        deepdrift.py <model file> <dataset file>
                 [--with-label WITH_LABEL]
                 [--window-sizes [WINDOW_SIZES [WINDOW_SIZES ...]]]
                 [--protein-layers [PROTEIN_LAYERS [PROTEIN_LAYERS ...]]]
                 [--drug-layers [DRUG_LAYERS [DRUG_LAYERS ...]]]
                 [--fc-layers [FC_LAYERS [FC_LAYERS ...]]]
                 [--learning-rate LEARNING_RATE] [--n-epoch N_EPOCH]
                 [--prot-vec PROT_VEC] [--prot-len PROT_LEN]
                 [--drug-vec DRUG_VEC] [--drug-len DRUG_LEN]
                 [--activation ACTIVATION] [--dropout DROPOUT]
                 [--filters N_FILTERS] [--batch-size BATCH_SIZE]
                 [--decay DECAY]
    Exmaple:

        python predict.py ./model.model -n predict -i ./toy_examples/test_dataset/test_dti.csv -d ./toy_examples/test_dataset/test_compound.csv -t ./toy_examples/test_dataset/test_protein.csv -v Convolution -l 2500 -V morgan_fp_r2 -L 2048 -W -o test_result.csv


### Dataset file format

    |   Protein ID   |  Sequence   | Compound ID    |   SMILES   |  Kd   |
    | ---------------|-------------|----------------|------------|-------|
    |  CHEMBL205     |  MSHHWGY... |  CHEMBL26590   | CC1(C)O... | 300   |
    |  ...           |  ...        |  ...           | ...        |       |
    |  CHEMBL1163124 |  MANFQEH... |  CHEMBL1289926 | CNC(=O)... | 10000 |

## Contact 

juw1179@psu.edu
