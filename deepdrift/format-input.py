from rdkit import Chem
from rdkit.Chem import AllChem
import sys
import re

print(sys.argv)

inputfile = sys.argv[1]
dtifile = sys.argv[2]
compoundfile = sys.argv[3]
proteinfile = sys.argv[4]

def smi2fp(smi):
  m = Chem.MolFromSmiles(smi)
  fp2 = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits=2048)
  return fp2.ToBitString()

seq_rdic = ['A','I','L','V','F','W','Y','N','C','Q','M','S','T','D','E','R','H','K','G','P','O','U','X','B','Z']

def formatsequence(seq):
  n = len(seq)
  for i in range(n):
    if seq[i].upper() not in seq_rdic:
      return seq[:i]
  return seq

inputs = []
for line in open(inputfile):
  l = re.split("\s+", line.strip())
  if len(l) >= 2:
    inputs.append(l)

f = open(dtifile, "w+")
print("Generate DTI File ...")
f.write(",Protein_ID,Compound_ID\n")
n=1
for ipt in inputs:
  f.write("{0},{1},{2}\n".format(n, n, n))
  n += 1
f.close()

f = open(compoundfile, "w+")
print("Generate Compound File ...")
f.write(",Compound_ID,morgan_fp_r2\n")
n=1
for ipt in inputs:
  f.write("{0},{1},{2}\n".format(n, n, '\t'.join(smi2fp(ipt[0]))))
  n += 1
f.close()

f = open(proteinfile, "w+")
print("Generate Protein File ...")
f.write(",Protein_ID,Sequence\n")
n=1
for ipt in inputs:
  f.write("{0},{1},{2}\n".format(n, n, formatsequence(ipt[1])))
  n += 1
f.close()
