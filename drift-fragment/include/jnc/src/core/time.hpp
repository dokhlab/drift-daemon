/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include <chrono>
#include <iostream>
#include "mpi.hpp"
#include "macros.hpp"

namespace jnc {

inline double dtime() {
    return std::chrono::duration_cast<std::chrono::duration<double>>
        (std::chrono::system_clock::now().time_since_epoch()).count();
}

inline double &last_time() {
    thread_local static double s_last_time;
    return s_last_time;
}

inline void tic() {
    last_time() = dtime();
}

inline void toc(std::ostream &stream = std::cout) {
    stream << "Elapsed time is " << dtime() - last_time() << " seconds." << std::endl;
}

struct Timer {
    double last_time;

    Timer() {
        last_time = dtime();
    }

    double toc() {
        double t = last_time;
        last_time = dtime();
        return last_time - t;
    }
};


}

