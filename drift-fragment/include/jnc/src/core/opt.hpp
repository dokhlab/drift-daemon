#pragma once

#include <string>
#include <fstream>
#include <deque>
#include <map>
#include <list>
#include <vector>
#include <iostream>
#include <type_traits>

#include "traits.hpp"

namespace jnc {

class Opt {
public:
    M<Str, Qs> pars;
    M<Str, Str> intros;
    Qs g;

    int argc;
    char **argv;

    Opt() = default;

    Opt(const Opt &par) = default;

    Opt(int argc, char **argv);

    Opt(Str str);

    void read(int argc, char **argv);

    void read(Str par_file);

    friend STD_ ostream &operator <<(STD_ ostream &out, const Opt &par);

    template<typename K>
    bool has(K &&k) const {
        return pars.find(k) != pars.end();
    }

    template<typename K, typename U, typename... V>
    bool has(K &&k, U &&u, V && ...rest) const {
        return has(k) || has(u, rest...);
    }

    template<typename T>
    void set(T &&v) const {}

    template<typename T, typename K, typename... V>
    void set(T &&v, K &&s, V && ...rest) const {
        if (s != "" && pars.find(s) != pars.end()) {
            v = parse<typename STD_ decay<T>::type>(pars.at(s)[0]);
        }
        else set(v, rest...);
    }

    template<typename T>
    void setv(T &&v) const {}

    template<typename T, typename _First, typename... _Rest>
    void setv(T &&t, _First &&first, _Rest && ...rest) const {
        if (first != "" && pars.find(first) != pars.end()) {
            auto && l = pars.at(first);
            t.resize(l.size());
            STD_ copy(l.begin(), l.end(), t.begin());
        }
        else {
            setv(t, rest...);
        }
    }

    STD_ list<Str> &keys_chain() const {
        static STD_ list<Str> chain;
        return chain;
    }

    Str get() const {
        STD_ ostringstream stream;
        stream << "jian::Opt::get error! Didn't found parameters for keys:";
        for (auto && key : keys_chain()) stream << " " << key;
        throw stream.str();
    }

    template<typename K, typename... V>
    Str get(K &&s, V && ..._pars) const {
        if (pars.count(s)) {
            keys_chain().clear();
            return pars.at(s)[0];
        } else {
            keys_chain().push_back(s);
            return get(_pars...);
        }
    }

    Qs getv() const {
        STD_ ostringstream stream;
        stream << "jian::Opt::getv error! Didn't found parameters for keys:";
        for (auto && key : keys_chain()) stream << " " << key;
        throw stream.str();
    }

    template<typename K, typename... V>
    Qs getv(K &&s, V && ..._pars) const {
        if (pars.count(s)) {
            keys_chain().clear();
            return pars.at(s);
        }
        else {
            keys_chain().push_back(s);
            return getv(_pars...);
        }
    }

    template<typename T>
    T parse(const Str &s) const {
        return lexical_cast<T>(s);
    }

};

} // namespace jnc

