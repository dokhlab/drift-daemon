#pragma once

#include "pdb_residue.hpp"

namespace jnc {

namespace pdb {

class Chain : public std::vector<Residue> {
public:
    std::string name;

    Chain() {}

    Chain(const std::string &filename) {
        read(filename);
    }

    void read(const std::string &filename);

    std::vector<const Atom *> patoms() const;

    std::vector<Atom *> patoms();

};

std::ostream &operator <<(std::ostream &output, const Chain &chain);

} // namespace pdb

} // namespace jnc

